-- Common Table Expression (CTE) to calculate sales data for weeks 49, 50 and 51 of 1999
WITH SalesData AS (
    SELECT
        t.calendar_week_number, -- Extracting calendar week number
        s.time_id,              -- Extracting time_id
        t.day_name,             -- Extracting day name
        SUM(s.amount_sold) AS sales -- Calculating total sales for the given day
    FROM
        sh.sales s
    JOIN
        sh.times t ON s.time_id = t.time_id -- Joining sales and times tables on time_id
    WHERE
        t.calendar_year = 1999 AND -- Filtering for the year 1999
        t.calendar_week_number IN (49, 50, 51) -- Filtering for weeks 49, 50 and 51
    GROUP BY
        t.calendar_week_number, s.time_id, t.day_name -- Grouping by week, time_id, and day name
),
-- CTE for calculating cumulative sales
CumulativeSales AS (
    SELECT
        calendar_week_number,
        time_id,
        day_name,
        sales,
        SUM(sales) OVER (
            PARTITION BY calendar_week_number -- Calculating cumulative sum for each week
            ORDER BY time_id
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ) AS CUM_SUM
    FROM
        SalesData
),
-- CTE for calculating centered 3-day average with adjusted window frames
CenteredAvg AS (
    SELECT
        calendar_week_number,
        time_id,
        day_name,
        sales,
        CUM_SUM,
        ROUND(
            CASE
                WHEN day_name = 'Monday' AND calendar_week_number = 49 THEN AVG(sales) OVER (
                    ORDER BY time_id
                    ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING
                )
                WHEN day_name = 'Friday' AND calendar_week_number = 51 THEN AVG(sales) OVER (
                    ORDER BY time_id
                    ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
                )
                WHEN day_name = 'Monday' THEN AVG(sales) OVER (
                    ORDER BY time_id
                    ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING
                )
                WHEN day_name = 'Friday' THEN AVG(sales) OVER (
                    ORDER BY time_id
                    ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING
                )
                ELSE AVG(sales) OVER (
                    ORDER BY time_id
                    ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
                )
            END, 2) AS CENTERED_3_DAY_AVG -- Calculating and rounding centered 3-day average TO hundereds
    FROM
        CumulativeSales
)
-- Final selection of required columns
SELECT
    calendar_week_number,
    time_id,
    day_name,
    sales,
    CUM_SUM,
    CENTERED_3_DAY_AVG
FROM
    CenteredAvg
-- Ordering by time_id to support chronological sequance
ORDER BY
    time_id;
